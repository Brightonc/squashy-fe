export const environment = {
  production: true,
  baseUrl: 'http://localhost:8000',
  baseUrlKey: 'BASE_URL_KEY'
};
