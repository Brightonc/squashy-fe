import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { emailExitsValidator } from '../../../shared/components/organisms/form/validators/validator';
import { UserModel } from '../../../shared/models/user.model';
import { slideRightToLeft, slideTopToBottom } from '../../../shared/animations/animations';


export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState( control: FormControl | null, form: FormGroupDirective | NgForm | null ): boolean {
    const isSubmitted = form && form.submitted;
    return !!( control && control.invalid && ( control.dirty || control.touched || isSubmitted ) );
  }
}

@Component( {
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: [ './user-signup.component.scss', '../account.component.scss' ],
  animations: [ slideTopToBottom, slideRightToLeft ]
} )
export class UserSignupComponent implements OnInit {

  hide = true;
  signUpForm: FormGroup;
  matcher = new ErrorStateMatcher();

  constructor() {
  }

  ngOnInit(): void {
    this.initSignUpForm();
  }

  onSubmit() {
    console.log( this.signUpForm );

    if ( this.signUpForm.valid ) {
      const user = new UserModel( this.signUpForm.value.fullName, this.signUpForm.value.email, this.signUpForm.value.password );
    }
  }

  initSignUpForm = () => {
    this.signUpForm = new FormGroup( {
      fullName: new FormControl( null, [ Validators.required ] ),
      email: new FormControl( null, [ Validators.required, Validators.email ], [ emailExitsValidator ] ),
      password: new FormControl( null, [ Validators.required, Validators.minLength( 6 ) ] )
    } );
  }

}
