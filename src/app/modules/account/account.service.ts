import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, UserModel } from '../../shared/models/user.model';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

const hey = gql`
  query user {
    name
  }
`

@Injectable( { providedIn: 'root' } )
export class AccountService {

  constructor( private http: HttpClient, private apollo: Apollo ) {
  }

  loginUser( email: string, password: string ) {
    return this.apollo.query(
      {
      query: gql`
        {
          user(id: 2) {
            firstName
            lastName
          }
        }
      `
    } );
    // this.http.post('/login', {email, password});
  }

  signUpUser( user: User ) {

    return this.http.post( '/sign-up', new UserModel( user.fullName, user.email, user.password ) );
  }

  resetPassword( email: string ) {
    return this.http.post( '/reset-password', { email } );
  }
}
