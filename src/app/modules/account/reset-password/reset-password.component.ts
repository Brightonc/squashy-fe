import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { ErrorStateMatcher } from '@angular/material/core';
import { slideTopToBottom } from '../../../shared/animations/animations';

@Component( {
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: [ './reset-password.component.scss', '../account.component.scss' ],
  animations: [
    slideTopToBottom
  ]
} )
export class ResetPasswordComponent implements OnInit {

  resetPasswordForm: FormGroup;
  matcher = new ErrorStateMatcher();

  constructor( ) {
  }

  ngOnInit(): void {
    this.initLoginForm();
  }

  onSubmit() {
    console.log( this.resetPasswordForm );
    if ( this.resetPasswordForm.valid ) {
      const email = this.resetPasswordForm.value.email;
    }
  }

  initLoginForm = () => {
    this.resetPasswordForm = new FormGroup( {
      email: new FormControl( null, [ Validators.required ] ),
    } );
  }


}
