import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

import { slideLeftToRight, slideRightToLeft } from '../../../shared/animations/animations';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

@Component( {
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: [ './user-login.component.scss', '../account.component.scss' ],
  animations: [
    slideLeftToRight,
    slideRightToLeft
  ]
} )
export class UserLoginComponent implements OnInit {

  hide = true;
  loginForm: FormGroup;
  matcher = new ErrorStateMatcher();

  constructor( private apollo: Apollo ) {
  }

  ngOnInit(): void {
    this.initLoginForm();
  }

  onSubmit() {
    if ( this.loginForm.valid ) {
      const credentials = { email: this.loginForm.value.email, password: this.loginForm.value.password };
    }


    this.apollo.query(
      {
      query: gql`
        {
          user(id: 2) {
            firstName
            lastName
          }
        }
      `
    } ).subscribe( resp =>
      console.log(resp.data)
    );

  }

  initLoginForm = () => {
    this.loginForm = new FormGroup( {
      email: new FormControl( null, [ Validators.required ] ),
      password: new FormControl( null, [ Validators.required ] )
    } );
  }
}
