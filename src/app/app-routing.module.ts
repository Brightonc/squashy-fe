import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './modules/home/home.component';
import { UserSignupComponent } from './modules/account/user-signup/user-signup.component';
import { UserLoginComponent } from './modules/account/user-login/user-login.component';
import { AccountComponent } from './modules/account/account.component';
import { ResetPasswordComponent } from './modules/account/reset-password/reset-password.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  {
    path: 'account', component: AccountComponent, children: [
      { path: '', redirectTo: '/account/login', pathMatch: 'full' },
      { path: 'sign-up', component: UserSignupComponent  },
      { path: 'login', component: UserLoginComponent },
      { path: 'reset-password', component: ResetPasswordComponent}
    ]
  }

];

@NgModule( {
  imports: [ RouterModule.forRoot( routes ) ],
  exports: [ RouterModule ]
} )
export class AppRoutingModule {
}
