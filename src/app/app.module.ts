import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './modules/home/home.component';
import { UserSignupComponent } from './modules/account/user-signup/user-signup.component';
import { UserLoginComponent } from './modules/account/user-login/user-login.component';
import { AccountComponent } from './modules/account/account.component';
import { ResetPasswordComponent } from './modules/account/reset-password/reset-password.component';
import { RequestInterceptor } from './core/interceptors/request.interceptor';
import { GraphQLModule } from './graphql.module';

@NgModule( {
  declarations: [
    AppComponent,
    HomeComponent,
    UserSignupComponent,
    UserLoginComponent,
    AccountComponent,
    ResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    GraphQLModule,
  ],
  providers: [ { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true } ],
  bootstrap: [ AppComponent ]
} )
export class AppModule {
}
