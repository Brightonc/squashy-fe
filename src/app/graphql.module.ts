import { NgModule } from '@angular/core';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import setContext from 'apollo-link/lib/test-utils/setContext';
import { HttpHeaders } from '@angular/common/http';

const uri = '/graphql'; // <-- add the URL of the GraphQL server here
export function createApollo( httpLink: HttpLink ) {

  const middleware = new setContext( () => ( {
    // tslint:disable-next-line:max-line-length
    header: new HttpHeaders().set( 'Authorization', 'yJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjEsImlhdCI6MTU4Mjg3ODYxOSwiZXhwIjoxNTgyOTY1MDE5LCJzdWIiOiJTYWxseSBKIiwic3UiOmZhbHNlfQ.83q5X5bstjKZpvnQBdo2nv1JTFpiw56FlTfFsecvLcc' )
  } ) );


  const linkY = middleware.concat( httpLink.create( { uri } ) );
  console.log(linkY);

  return {
    link: linkY,
    cache: new InMemoryCache(),
  };
}

@NgModule( {
  exports: [ ApolloModule, HttpLinkModule ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [ HttpLink ],
    },
  ],
} )
export class GraphQLModule {
}
