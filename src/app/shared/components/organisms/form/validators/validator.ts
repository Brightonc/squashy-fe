import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';


export const emailExitsValidator = ( control: FormControl ): Promise<any> | Observable<any> => {

  const promise = new Promise( resolve => {
      setTimeout( () => {
        if ( control.value === 'bchabula@gmail.com' ) {
          resolve( { emailExist: true } );
        } else {
          resolve(null);
        }
      }, 2000 );
    });

  return promise;
};
