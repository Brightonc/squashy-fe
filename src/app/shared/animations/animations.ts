import { animate, state, style, transition, trigger } from '@angular/animations';

const normal = 500;
const fast = 300;

export const slideLeftToRight = trigger( 'slideLeftToRight', [
  state( 'in', style( {
    opacity: 1,
    transform: 'translateX(0)'
  } ) ),

  transition( 'void => *', [
    style( {
      opacity: 0,
      transform: 'translateX(-200px)'
    } ),
    animate( fast )
  ] )
] );


export const slideTopToBottom = trigger( 'slideTopToBottom', [
  state( 'in', style( {
    opacity: 1,
    transform: 'translateX(0)'
  } ) ),

  transition( 'void => *', [
    style( {
      opacity: 0,
      transform: 'translateY(-200px)'
    } ),
    animate( fast )
  ] )
] );


export const slideRightToLeft = trigger( 'slideRightToLeft', [
  state( 'in', style( {
    opacity: 1,
    transform: 'translateX(0)'
  } ) ),

  transition( 'void => *', [
    style( {
      opacity: 0,
      transform: 'translateX(100px)'
    } ),
    animate( fast )
  ] ),

  transition( '* => void', [
    animate( fast, style( {
      opacity: 0,
      transform: 'translateX(-100px)'
    } ) )
  ] )
] );
