export class UserModel {

  constructor(
    public fullName: string,
    public email: string,
    public password: string
  ) {
  }
}

export interface User {
  fullName: string;
  email: string;
  password: string;
}
